#1
#enable switch configuration removing "#" from /usr/local/nagios/etc/nagios.cfg file in the following line
#cfg_file=/usr/local/nagios/etc/objects/switch.cfg

#2
#open the file /usr/local/nagios/etc/objects/switch.cfg to add the host to monitor

define host{

	use		generic-switch		; Inherit default values from a template

	host_name		model_name		; The name we're giving to this switch

	alias		alias_name	; A longer name associated with the switch

	address		192.168.1.253		; IP address of the switch

	hostgroups	allhosts,switches			; Host groups this switch is associated with

}

#3
#define the snmp service to monitor, this example is to monitor the up time of the switch
define service{

	use			generic-service	; Inherit values from a template

	host_name			model_name

	service_description	Uptime	

	check_command		check_snmp!-C public -o sysUpTime.0

	}
	
#4
#restart nagios to start the monitoring
/etc/rc.d/init.d/nagios reload